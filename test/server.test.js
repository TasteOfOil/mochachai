const assert = require('chai').assert;

var lesson = {
    lessonName: "Russian",
    teacherName: "Galina Mihailovna",
    studentCount: 140
};

var newLesson = {
    lessonName: "English"
};

describe('Test server.js', ()=>{
    it('GET lesson', async ()=>{
        const getLesson = await fetch('http://localhost:3000/api/lessons/6').then(res => res.json()).catch(err=>'error');
        assert.equal(getLesson[0].lessonname,"English")
    });
    it('POST lesson', async()=>{
        const result = await fetch('http://localhost:3000/api/lessons', {
            method: 'POST',
            body: JSON.stringify(lesson),
            headers: { 'Content-Type': 'application/json' }
        }).then(res => res.text());
        
        assert.equal(result, 'Data added successfully')
    });
    it('PUT lesson', async()=>{
        const result = await fetch('http://localhost:3000/api/lessons/6', {
            method: 'PUT',
            body: JSON.stringify(newLesson),
            headers: { 'Content-Type': 'application/json' }
        }).then(res => res.text());
        
        assert.equal(result, "Data updated successfully");
    });
    it('DELETE lesson', async()=>{
        const result = await fetch('http://localhost:3000/api/lessons/8', {
            method: 'DELETE'
        }).then(res => res.text());
        assert(result, "Data deleted successfully");
    });
    it('GET lessons', async ()=>{
        const getLessons = await fetch('http://localhost:3000/api/lessons').then(res => res.json()).catch(err=>'error');
        assert.notInclude(getLessons, {
            id: 9,
            lessonname: "Geometry",
            teachername: "Michail Petrovich",
            studentcount: 140
        });
    });
});




